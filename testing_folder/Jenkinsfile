def remote = [:]
remote.host = 'veefil-666.tritium.com.au'
remote.user = 'tritium'

def artifactoryServerId = 'Jfrog-tritium'
def artifactoryServerUrl = 'https://tritium.jfrog.io/artifactory'
def artifactoryServerCredentialsId = 'd12b4bc3-517b-4e7d-bd7a-b657c70e4e70'
def artifactoryRepository = 'docker-local-test'
def artifactoryDockerRegistry = 'tritium.jfrog.io'
def imageName = 'veefil-sdcard'
def directory = ''
def reportId = ''

def TeamsWebhook = "https://outlook.office.com/webhook/3e6fdd1d-f9af-4a9a-ae03-c8cd6584a47c@058dc2ae-7133-457e-8a7d-b864fe02fae7/JenkinsCI/a25f59a8447449aeb00ea0a30578ca6a/91b7bb8e-97d9-407e-b0ab-523306aed905"

pipeline {
    agent any

    stages {

        stage('Rootfs Download') {
            steps {
                sh 'rm -rf veefil_sdcard'
                sh 'mkdir veefil_sdcard'
		sh 'pwd'
		sh "scp -o StrictHostKeyChecking=no testing_folder/xray_scan_exclude.txt ${remote.user}@${remote.host}:/home/tritium/"
                //sh "ssh ${remote.user}@${remote.host} -o StrictHostKeyChecking=no tar zcf - -X xray_scan_exclude.txt /etc/ssh > veefil_sdcard.tgz"
                sh "ssh ${remote.user}@${remote.host} -o StrictHostKeyChecking=no tar zcf - -X xray_scan_exclude.txt /usr /bin /opt /etc > veefil_sdcard.tgz"
                sh 'tar -zxf veefil_sdcard.tgz -C veefil_sdcard/ '
            }
        }

        stage('Artifactory configuration') {
            steps {
                rtServer(
                    id: artifactoryServerId,
                    url: artifactoryServerUrl,
                    credentialsId: artifactoryServerCredentialsId
                )
            }
        }

        stage('Docker build image') {
            steps {
                sh 'echo "FROM scratch\nADD . /" > veefil_sdcard/Dockerfile'
                script {
                    docker.build(artifactoryDockerRegistry + "/${artifactoryRepository}/" + imageName + ":${env.BUILD_ID}","./veefil_sdcard/")
                }
            }
        }

        stage('Push Image to Artifactory') {
            steps {
                rtDockerPush(
                    serverId: artifactoryServerId,
                    image: artifactoryDockerRegistry + "/${artifactoryRepository}/" + imageName + ":${env.BUILD_ID}",
                    targetRepo: artifactoryRepository
                )
            }
        }

        stage('Publish build info') {
            steps {
                rtPublishBuildInfo(
                    serverId: artifactoryServerId
                )
            }
        }

        stage('Xray scan') {
            steps {
                xrayScan(
                    serverId: artifactoryServerId,
                    failBuild: false
                )
            }
        }

        stage('capture console output') {
            steps {
                script {
                    directory = "${env.WORKSPACE}/jfrog-xray-scan" 
                    def logContent = Jenkins.getInstance().getItemByFullName(env.JOB_NAME).getBuildByNumber(Integer.parseInt(env.BUILD_NUMBER)).logFile.text
                    writeFile file: directory + "/buildConsolelog.txt",
                    text: logContent
                    def consoleOutput = readFile directory + '/buildConsolelog.txt'
                }
                // TODO : Build_ID to something more appropriate , maybe date ?
                sh "cat $directory/buildConsolelog.txt |  sed -ne '/xrayScan/,\$ p'  > xray_scan_output.${env.BUILD_ID}"
            }
        }

        stage('Generate XRAY report') {
            steps {
                script {
                    withCredentials([usernameColonPassword(credentialsId: "d12b4bc3-517b-4e7d-bd7a-b657c70e4e70", variable: "API_TOKEN")]) {
                        url =  "-u $API_TOKEN -X POST -H \"Content-Type: application/json\" -d '{\"name\": \"$imageName-${env.BUILD_ID}.report\", \"resources\": { \"repositories\": [ { \"name\": \"$artifactoryRepository\", \"include_path_patterns\": [ \"*$imageName/${env.BUILD_ID}/*\" ] } ] } }'  https://tritium.jfrog.io/xray/api/v1/reports/vulnerabilities"
                        response = sh(script: "curl $url", returnStdout: true).trim()
                        reportId = sh(script: "echo $response | cut -f1 -d\",\" | cut -f2 -d\":\"", returnStdout: true).trim()
                        echo reportId
                    }
                }
            }
        }

        stage('Download XRAY report') {
            steps {
                script {
                    sleep 3 // Sleep introduced for JFrog to generate the reports
                    withCredentials([usernameColonPassword(credentialsId: "d12b4bc3-517b-4e7d-bd7a-b657c70e4e70", variable: "API_TOKEN")]) {
                        url = "-u ${API_TOKEN} -o ${imageName}-${env.BUILD_ID}-${reportId}.zip  'https://tritium.jfrog.io/xray/api/v1/reports/export/${reportId}?file_name=xray_scan_report.${env.BUILD_ID}_${reportId}&format=pdf'"
                        response = sh(script: "curl $url", returnStdout: true).trim()
                        sh "unzip ${imageName}-${env.BUILD_ID}-${reportId}.zip"
                    }
                }
            }
        }

        stage('Remove docker image') {
            steps {
                sh "docker rmi tritium.jfrog.io/docker-local-test/$imageName:${env.BUILD_ID}"
                sh "rm -f ${imageName}-${env.BUILD_ID}-${reportId}.zip"
            }
        }

        stage ('Checkout Xray scan repository') {
            steps {
                sshagent(['77229559-f597-433b-8a7b-27cd80fe325d']) {

                    // Remove the directory to make sure checkout is successfull.
                    sh "rm -rf tritium-scan-results/"
                    sh "git clone git@bitbucket.org:tritiumdevops/tritium-scan-results.git"
                    sh "mv xray_scan_output.${env.BUILD_ID} tritium-scan-results/"
                    sh "mv xray_scan_report.${env.BUILD_ID}_${reportId}.pdf tritium-scan-results/"
                             
                    dir('tritium-scan-results') {
                        sh "git config user.email veera.datla@tritium.com.au"
                        sh "git config user.name Veera Datla"
                        sh "git add xray_scan_output.${env.BUILD_ID}"
                        sh "git add xray_scan_output.${env.BUILD_ID}_${reportId}.pdf"
                        sh "git commit -m \"Jenkins xray scan report publish\""
                        sh "git push origin HEAD"
                    }
                }
            }
        }
    }

    post{
        success {
            emailext(
                subject:"${env.JOB_NAME} - #${env.BUILD_NUMBER} - Build Success!",
                //  no indents on email text due to affecting email layout
                body:'''
Build Info
Build URL: ${BUILD_URL}
Project:${JOB_NAME}

Changes:
${CHANGES}

Changes Since Last Success:
${CHANGES_SINCE_LAST_SUCCESS}

Test Results:
Total: ${TEST_COUNTS,var="total"}
Passed: ${TEST_COUNTS,var="pass"}
Failed: ${TEST_COUNTS,var="fail"}
''',
                recipientProviders: [[$class: 'DevelopersRecipientProvider'],[$class: 'RequesterRecipientProvider']]
            )
            office365ConnectorSend status: 'Build Success', webhookUrl: "${TeamsWebhook}"
        }
        failure {
            emailext(
                subject:"${env.JOB_NAME} - #${env.BUILD_NUMBER} - Build Failed!",
                //  no indents on email text due to affecting email layout
                body:'''
Build Info
Build URL: ${BUILD_URL}
Project:${JOB_NAME}

Changes:
${CHANGES}

Changes Since Last Success:
${CHANGES_SINCE_LAST_SUCCESS}

Test Results:
Total: ${TEST_COUNTS,var="total"}
Passed: ${TEST_COUNTS,var="pass"}
Failed: ${TEST_COUNTS,var="fail"}
''',
                recipientProviders: [[$class: 'DevelopersRecipientProvider'],[$class: 'RequesterRecipientProvider']]
            )
            office365ConnectorSend status: 'Build Failed', webhookUrl: "${TeamsWebhook}"  
        }
    }
}

